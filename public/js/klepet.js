var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.zasebnoSporocilo = function(prejemnik, zasebnoSporocilo){
  var sporocilo = {
    prejemnik: prejemnik,
    zasebnoSporocilo: zasebnoSporocilo
  };
  this.socket.emit('posljiZasebnoSporociloZahteva', sporocilo);
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var prejemnik = besede[0].substring(1, besede[0].length - 1);
      besede.shift();
      var zasebnoSporocilo = besede.join(' ');
      zasebnoSporocilo = zasebnoSporocilo.substring(1, zasebnoSporocilo.length-1);
      this.socket.emit('posljiZasebnoSporociloZahteva', {prejemnik: prejemnik, zasebnoSporocilo: zasebnoSporocilo});
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};