var tabelaGrdihBesed; 

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function zamenjajGrdeBesede(besedilo){
  for(var i = 0; i < tabelaGrdihBesed.length; i++){
    var regex = new RegExp("\\b" + tabelaGrdihBesed[i] + "\\b", "g");
    besedilo = besedilo.replace(regex, narediZvezdice(tabelaGrdihBesed[i]));
  }
  return besedilo;
}

function narediZvezdice(besedilo){
  var zvezdice = '';
  for(var i = 0; i < besedilo.length; i++){
    zvezdice += '*';
  }
  return zvezdice;
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function divElementSmajliTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }

  } else if(sporocilo.indexOf(':)') != -1 || sporocilo.indexOf('(y)') != -1 || sporocilo.indexOf(':(') != -1 || sporocilo.indexOf(':*') != -1 || sporocilo.indexOf(';)') != -1){
  
    if(sporocilo.indexOf('<') != -1 || sporocilo.indexOf('>') != -1){
      sporocilo = sporocilo.replace(/</g, '&lt');
      sporocilo = sporocilo.replace(/>/g, '&gt');
    }
    if(sporocilo.indexOf(':)') != -1){
      sporocilo = sporocilo.replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"></img>');
    }
    if(sporocilo.indexOf(':(') != -1){
      sporocilo = sporocilo.replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"></img>');
    }
    if(sporocilo.indexOf(':*') != -1){
      sporocilo = sporocilo.replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"></img>');
    }
    if(sporocilo.indexOf(';)') != -1){
      sporocilo = sporocilo.replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"></img>');
    }
    if(sporocilo.indexOf('(y)') != -1){
      sporocilo = sporocilo.replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"></img>');
    }
    
    sporocilo = zamenjajGrdeBesede(sporocilo);
    
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementSmajliTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight')); 
  }
 else {
    sporocilo = zamenjajGrdeBesede(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

function vzdevekKanal(vzdevek, kanal){
  $('#kanal').text(vzdevek + ' @ ' + kanal);
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  socket.on('tabelaBesed', function(tabelaBesed){
    tabelaGrdihBesed = tabelaBesed;
  });

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    if(rezultat.kanal == undefined){
     $('#kanal').text(rezultat.vzdevek);
    }
    else{
      vzdevekKanal(rezultat.vzdevek, rezultat.kanal);
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  socket.on('zasebnoSporociloOdgovor', function(rezultat){
    var sporocilo;
    if(rezultat.uspesno){
      sporocilo = rezultat.zasebno;
    }
    else{
      sporocilo = rezultat.zasebno;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    var kanal = rezultat.kanal;
    if(rezultat.vzdevek.search('Gost') != -1){
      $('#kanal').append(' @ ' + kanal);
    }
    else{
      vzdevekKanal(rezultat.vzdevek, rezultat.kanal);
    }
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  socket.on('uporabnikiNaKanalu', function(uporabniki){
   // var tekst;
     $('#seznam-uporabnikov').empty();
    for(var i in uporabniki){
      //tekst = $('<div style="font-weight: bold"></div>').text(uporabnikiNaKanaluZaIzpis[i]);
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    }
  });
  
 /* socket.on('sporocilo', function(uporabniki) {
   // $('#seznam-uporabnikov').empty();
    
    for(var i in uporabniki.length) {
      if (uporabniki[i] != '') {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
    }
    
  });*/

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  
  setInterval(function() {
    socket.emit('uporabnikiNaKanalu');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});