var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZahtevoZaPrivatnoSporocilo(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    narediTabeloBesed(socket);
    socket.on('uporabnikiNaKanalu', function(){
      var kanal = trenutniKanal[socket.id];
      var uporabnikiNaKanalu = io.sockets.clients(kanal);
      var uporabnikiNaKanaluZaIzpis = [];
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        uporabnikiNaKanaluZaIzpis.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
        console.log(uporabnikiNaKanaluZaIzpis);
   }
   socket.emit('uporabnikiNaKanalu', uporabnikiNaKanaluZaIzpis);
    });
    
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function narediTabeloBesed(socket){
  var fs = require('fs');
  var array = fs.readFileSync('public/swearWords.txt').toString().split("\n");
  socket.emit('tabelaBesed', array);
}

function zamenjajGrdeBesede(besedilo){
  var fs = require('fs');
  var tabelaGrdihBesed = fs.readFileSync('public/swearWords.txt').toString().split("\n");
  for(var i = 0; i < tabelaGrdihBesed.length; i++){
    var regex = new RegExp("\\b" + tabelaGrdihBesed[i] + "\\b", "g");
    besedilo = besedilo.replace(regex, narediZvezdice(tabelaGrdihBesed[i]));
  }
  return besedilo;
}

function narediZvezdice(besedilo){
  var zvezdice = '';
  for(var i = 0; i < besedilo.length; i++){
    zvezdice += '*';
  }
  return zvezdice;
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek,
    vzdevki: uporabljeniVzdevki
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {
  kanal: kanal,
  vzdevek: vzdevkiGledeNaSocket[socket.id]   
  });
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {
      besedilo: uporabnikiNaKanaluPovzetek,
    });
}
}

function obdelajZahtevoZaPrivatnoSporocilo(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki){
  socket.on('posljiZasebnoSporociloZahteva', function(sporocilo){
    
    var msg = sporocilo.zasebnoSporocilo;
    
    if(msg.indexOf('<') != -1 || msg.indexOf('>') != -1){
      msg = msg.replace(/</g, '&lt');
      msg = msg.replace(/>/g, '&gt');
    }
    if(msg.indexOf(':)') != -1){
      msg = msg.replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"></img>');
    }
    if(msg.indexOf(':(') != -1){
      msg = msg.replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"></img>');
    }
    if(msg.indexOf(':*') != -1){
      msg = msg.replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"></img>');
    }
    if(msg.indexOf(';)') != -1){
      msg = msg.replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"></img>');
    }
    if(msg.indexOf('(y)') != -1){
      msg = msg.replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"></img>');
    }
    
    msg = zamenjajGrdeBesede(msg);
    
    if(sporocilo.prejemnik == vzdevkiGledeNaSocket[socket.id] || uporabljeniVzdevki.indexOf(sporocilo.prejemnik) == -1){
      socket.emit('zasebnoSporociloOdgovor', {
        uspesno: false,
        zasebno: 'Sporočila '+ msg +' uporabniku z vzdevkom '+ sporocilo.prejemnik +' ni bilo mogoče posredovati.'
      });
    }
    else{
      var kanali = io.sockets.manager.rooms;
      for(var i in kanali){
        var kanal = i.substring(1, i.length);
        var uporabnikiNaKanalu = io.sockets.clients(kanal);
        for(var j in uporabnikiNaKanalu){
          if(sporocilo.prejemnik == vzdevkiGledeNaSocket[uporabnikiNaKanalu[j].id] ){
            
            socket.emit('zasebnoSporociloOdgovor', {
              uspesno: true,
              zasebno: '(Zasebno za ' + sporocilo.prejemnik + '): ' + msg
            });
            
            io.sockets.sockets[uporabnikiNaKanalu[j].id].emit('sporocilo', {
              besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + msg
            });
            
          }
        }
      }
    }
    
  });
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: trenutniKanal[socket.id]
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.novKanal);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}