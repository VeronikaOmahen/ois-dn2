# 2. domača naloga pri predmetu OIS 2014-2015 #

Namen 2. domače naloge je demonstracija primera **spletne klepetalnice** z naslednjim ciljem:

* pregled različnih Node.js komponent,
* primer polno delujoče aplikacije s pomočjo tehnologije Node,
* asinhrona interakcija med odjemalcem in strežnikom.

## Funkcionalnosti##

* vnos sporočil v klepetalnico, ki se nato posredujejo vsem prijavljenim uporabnikom.
* sprememba vzdevka uporabnika,
* sprememba kanala na klepetalnici.

## Tehnične podrobnosti##

* strežba **statičnih datotek** (npr. HTML, CSS in JavaScript na strani odjemalca),
* obvladovanje **asinhronega pošiljanja sporočil** med strežnikom in odjemalci (WebSocket).

## Naloga_2_1

*dodajanje smajlijev

## Naloga_2_2

* prikaz vzdevka
* 
## Naloga_2_3

*filter besed

* 
## Naloga_2_4

* stilska preobrazba

* 
## Naloga_2_5
* dodajanje seznama uporabnikov

* 
## Naloga_2_6 

*zasebna sporočila
